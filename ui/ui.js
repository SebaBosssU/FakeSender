define('two/faker/ui', ['two/faker', 'two/locale', 'two/ui', 'two/ui/autoComplete', 'two/FrontButton', 'two/utils', 'two/eventQueue', 'ejs', 'struct/MapData', 'cdn'], function(FakeSender, Locale, Interface, autoComplete, FrontButton, utils, eventQueue, ejs, $mapData, cdn) {
    var ui
    var opener
    var $window
    var $fourOfAKind
    var $fakePlayer
    var $fakeVillage
    var $rename
    var $renameCount
    /**
     * Bind all elements events and notifications.
     */
    var bindEvents = function() {
        $fourOfAKind.on('click', function() {
            var playerId1 = document.getElementById('fakes').value
            var modelDataService = injector.get('modelDataService')
            var socketService = injector.get('socketService')
            var routeProvider = injector.get('routeProvider')
            var player = modelDataService.getSelectedCharacter()
            var presetsList = modelDataService.getPresetList()
            var presets = presetsList.presets
            var villages = player.getVillageList()
            var interval = 5000
            var interval1 = 15000
            var FakePresets = []
            var presetsAssigned = []
            var TargetVillages = []

            function Presets() {
                socketService.emit(routeProvider.GET_PRESETS, {}, function(data) {
                    for (i = 0; i < data.presets.length; i++) {
                        if ((data.presets[i].name).indexOf('kareta') >= 0) {
                            FakePresets.push(data.presets[i].id)
                        }
                    }
                })
                setTimeout(targetList, 3000)
            }

            function targetList(id) {
                socketService.emit(routeProvider.CHAR_GET_PROFILE, {
                    character_id: playerId1
                }, function(data) {
                    for (i = 0; i < data.villages.length; i++) {
                        TargetVillages.push(data.villages[i].village_id)
                    }
                })
                setTimeout(assignedVillages, 3000)
            }

            function assignedVillages() {
                FakePresets.forEach(function(preset) {
                    socketService.emit(routeProvider.GET_PRESETS, {}, function(data) {
                        for (i = 0; i < data.presets.length; i++) {
                            if ((data.presets[i].name).indexOf('kareta') >= 0) {
                                presetsAssigned.push(data.presets[i].assigned_villages)
                            }
                        }
                    })
                })
                setTimeout(Fake0, 1000)
            }

            function Fake0() {
                if (!FakePresets[0]) {
                    utils.emitNotif('error', Locale('faker', 'no-presets'))
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[0].includes(VID)) {
                            utils.emitNotif('error', 'Szablon nie przypisany do: '+VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(0, 4)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[0],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[0],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[0],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[0],
                                            type: 'attack'
                                        })
                                        utils.emitNotif('success', 'Kareta fejkowa wysłana z: '+VID+' na: '+target+' z szablonu: '+FakePresets[0])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake1, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake1() {
                if (!FakePresets[1]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[1].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[1] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(4, 8)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[1],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[1],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[1],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[1],
                                            type: 'attack'
                                        })
                                        console.log('4 commands Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[1])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake2, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake2() {
                if (!FakePresets[2]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[2].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[2] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(8, 12)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[2],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[2],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[2],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[2],
                                            type: 'attack'
                                        })
                                        console.log('4 commands Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[2])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake3, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake3() {
                if (!FakePresets[2]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[3].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[3] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(12, 16)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[3],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[3],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[3],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[3],
                                            type: 'attack'
                                        })
                                        console.log('4 commands Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[3])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake4, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake4() {
                if (!FakePresets[4]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[4].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[4] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(16, 20)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[4],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[4],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[4],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[4],
                                            type: 'attack'
                                        })
                                        console.log('4 commands Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[4])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake5, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake5() {
                if (!FakePresets[5]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[5].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[5] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(20, 24)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[5],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[5],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[5],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[5],
                                            type: 'attack'
                                        })
                                        console.log('4 commands Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[5])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake6, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake6() {
                if (!FakePresets[6]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[6].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[6] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(24, 28)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[6],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[6],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[6],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[6],
                                            type: 'attack'
                                        })
                                        console.log('4 commands Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[6])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake7, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake7() {
                if (!FakePresets[7]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[7].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[7] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(28, 32)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[7],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[7],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[7],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[7],
                                            type: 'attack'
                                        })
                                        console.log('4 commands Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[7])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake8, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake8() {
                if (!FakePresets[8]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[8].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[8] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(32, 36)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[8],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[8],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[8],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[8],
                                            type: 'attack'
                                        })
                                        console.log('4 commands Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[8])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake9, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake9() {
                if (!FakePresets[9]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[9].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[9] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(36, 40)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[9],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[9],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[9],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[9],
                                            type: 'attack'
                                        })
                                        console.log('4 commands Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[9])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake10, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake10() {
                if (!FakePresets[10]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[10].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[10] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(40, 44)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[10],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[10],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[10],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[10],
                                            type: 'attack'
                                        })
                                        console.log('4 commands Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[10])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake11, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake11() {
                if (!FakePresets[11]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[11].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[11] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(44, 48)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[11],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[11],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[11],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[11],
                                            type: 'attack'
                                        })
                                        console.log('4 commands Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[11])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake12, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake12() {
                if (!FakePresets[12]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[12].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[12] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(48, 52)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[12],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[12],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[12],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[12],
                                            type: 'attack'
                                        })
                                        console.log('4 commands Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[12])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake13, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake13() {
                if (!FakePresets[13]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[13].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[13] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(52, 56)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[13],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[13],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[13],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[13],
                                            type: 'attack'
                                        })
                                        console.log('4 commands Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[13])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake14, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake14() {
                if (!FakePresets[14]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[14].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[14] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(56, 60)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[14],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[14],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[14],
                                            type: 'attack'
                                        })
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[14],
                                            type: 'attack'
                                        })
                                        console.log('4 commands Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[14])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                }
            }
            Presets()
        })
        $fakePlayer.on('click', function() {
            var playerId1 = document.getElementById('fakes1').value
            var modelDataService = injector.get('modelDataService')
            var socketService = injector.get('socketService')
            var routeProvider = injector.get('routeProvider')
            var player = modelDataService.getSelectedCharacter()
            var presetsList = modelDataService.getPresetList()
            var presets = presetsList.presets
            var villages = player.getVillageList()
            var interval = 4000
            var interval1 = 15000
            var FakePresets = []
            var presetsAssigned = []
            var TargetVillages = []

            function Presets() {
                socketService.emit(routeProvider.GET_PRESETS, {}, function(data) {
                    for (i = 0; i < data.presets.length; i++) {
                        if ((data.presets[i].name).indexOf('fejk') >= 0) {
                            console.log('Preset: ' + data.presets[i].id + ', Name: ' + data.presets[i].name)
                            FakePresets.push(data.presets[i].id)
                        }
                    }
                })
                setTimeout(targetList, 3000)
            }

            function targetList(id) {
                socketService.emit(routeProvider.CHAR_GET_PROFILE, {
                    character_id: playerId1
                }, function(data) {
                    for (i = 0; i < data.villages.length; i++) {
                        console.log('Id: ' + data.villages[i].village_id + ', Nazwa: ' + data.villages[i].village_name)
                        TargetVillages.push(data.villages[i].village_id)
                    }
                })
                setTimeout(assignedVillages, 3000)
            }

            function assignedVillages() {
                FakePresets.forEach(function(preset) {
                    socketService.emit(routeProvider.GET_PRESETS, {}, function(data) {
                        for (i = 0; i < data.presets.length; i++) {
                            if ((data.presets[i].name).indexOf('fejk') >= 0) {
                                presetsAssigned.push(data.presets[i].assigned_villages)
                            }
                        }
                    })
                })
                setTimeout(Fake0, 1000)
            }

            function Fake0() {
                if (!FakePresets[0]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[0].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[0] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(0, 6)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[0],
                                            type: 'attack'
                                        })
                                        console.log('Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[0])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake1, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake1() {
                if (!FakePresets[1]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[1].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[1] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(6, 12)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[1],
                                            type: 'attack'
                                        })
                                        console.log('Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[1])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake2, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake2() {
                if (!FakePresets[2]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[2].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[2] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(12, 18)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[2],
                                            type: 'attack'
                                        })
                                        console.log('Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[2])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake3, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake3() {
                if (!FakePresets[2]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[3].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[3] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(18, 24)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[3],
                                            type: 'attack'
                                        })
                                        console.log('Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[3])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake4, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake4() {
                if (!FakePresets[4]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[4].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[4] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(24, 30)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[4],
                                            type: 'attack'
                                        })
                                        console.log('Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[4])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake5, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake5() {
                if (!FakePresets[5]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[5].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[5] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(30, 36)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[5],
                                            type: 'attack'
                                        })
                                        console.log('Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[5])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake6, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake6() {
                if (!FakePresets[6]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[6].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[6] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(36, 42)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[6],
                                            type: 'attack'
                                        })
                                        console.log('Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[6])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake7, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake7() {
                if (!FakePresets[7]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[7].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[7] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(42, 48)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[7],
                                            type: 'attack'
                                        })
                                        console.log('Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[7])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake8, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake8() {
                if (!FakePresets[8]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[8].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[8] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(48, 54)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[8],
                                            type: 'attack'
                                        })
                                        console.log('Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[8])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake9, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake9() {
                if (!FakePresets[9]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[9].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[9] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(54, 60)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[9],
                                            type: 'attack'
                                        })
                                        console.log('Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[9])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake10, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake10() {
                if (!FakePresets[10]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[10].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[10] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(60, 66)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[10],
                                            type: 'attack'
                                        })
                                        console.log('Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[10])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake11, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake11() {
                if (!FakePresets[11]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[11].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[11] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(66, 72)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[11],
                                            type: 'attack'
                                        })
                                        console.log('Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[11])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake12, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake12() {
                if (!FakePresets[12]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[12].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[12] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(72, 78)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[12],
                                            type: 'attack'
                                        })
                                        console.log('Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[12])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake13, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake13() {
                if (!FakePresets[13]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[13].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[13] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(78, 84)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[13],
                                            type: 'attack'
                                        })
                                        console.log('Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[13])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake14, interval1 * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake14() {
                if (!FakePresets[14]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[14].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[14] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                Targets = TargetVillages.slice(84, 90)
                                Targets.forEach(function(target, index) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SEND_PRESET, {
                                            start_village: VID,
                                            target_village: target,
                                            army_preset_id: FakePresets[14],
                                            type: 'attack'
                                        })
                                        console.log('Village: ' + VID + ', Target: ' + target + ', Preset: ' + FakePresets[14])
                                    }, index * interval * Math.floor((Math.random() * 5) + 1))
                                })
                            }, index * interval * Math.random())
                        }
                    })
                }
            }
            Presets()
        })
        $fakeVillage.on('click', function() {
            var villageId = document.getElementById('fakes2').value
            var player = modelDataService.getSelectedCharacter()
            var presetsList = modelDataService.getPresetList()
            var presets = presetsList.presets
            var villages = player.getVillageList()
            var interval = 3000
            var FakePresets = []
            var presetsAssigned = []

            function Presets() {
                socketService.emit(routeProvider.GET_PRESETS, {}, function(data) {
                    for (i = 0; i < data.presets.length; i++) {
                        if ((data.presets[i].name).indexOf('fejk') >= 0) {
                            console.log('Preset: ' + data.presets[i].id + ', Name: ' + data.presets[i].name)
                            FakePresets.push(data.presets[i].id)
                        }
                    }
                })
                setTimeout(assignedVillages, 3000)
            }

            function assignedVillages() {
                FakePresets.forEach(function(preset) {
                    socketService.emit(routeProvider.GET_PRESETS, {}, function(data) {
                        for (i = 0; i < data.presets.length; i++) {
                            if ((data.presets[i].name).indexOf('fejk') >= 0) {
                                presetsAssigned.push(data.presets[i].assigned_villages)
                            }
                        }
                    })
                })
                setTimeout(Fake0, 1000)
            }

            function Fake0() {
                if (!FakePresets[0]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[0].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[0] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                socketService.emit(routeProvider.SEND_PRESET, {
                                    start_village: VID,
                                    target_village: villageId,
                                    army_preset_id: FakePresets[0],
                                    type: 'attack'
                                })
                                console.log('Village: ' + VID + ', Target: ' + villageId + ', Preset: ' + FakePresets[0])
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake1, interval * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake1() {
                if (!FakePresets[1]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[1].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[1] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                socketService.emit(routeProvider.SEND_PRESET, {
                                    start_village: village.getId(),
                                    target_village: villageId,
                                    army_preset_id: FakePresets[1],
                                    type: 'attack'
                                })
                                console.log('Village: ' + village.getId() + ', Target: ' + villageId + ', Preset: ' + FakePresets[1])
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake2, interval * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake2() {
                if (!FakePresets[2]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[2].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[2] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                socketService.emit(routeProvider.SEND_PRESET, {
                                    start_village: village.getId(),
                                    target_village: villageId,
                                    army_preset_id: FakePresets[2],
                                    type: 'attack'
                                })
                                console.log('Village: ' + village.getId() + ', Target: ' + villageId + ', Preset: ' + FakePresets[2])
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake3, interval * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake3() {
                if (!FakePresets[3]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[3].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[3] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                socketService.emit(routeProvider.SEND_PRESET, {
                                    start_village: village.getId(),
                                    target_village: villageId,
                                    army_preset_id: FakePresets[3],
                                    type: 'attack'
                                })
                                console.log('Village: ' + village.getId() + ', Target: ' + villageId + ', Preset: ' + FakePresets[3])
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake4, interval * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake4() {
                if (!FakePresets[4]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[4].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[4] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                socketService.emit(routeProvider.SEND_PRESET, {
                                    start_village: village.getId(),
                                    target_village: villageId,
                                    army_preset_id: FakePresets[4],
                                    type: 'attack'
                                })
                                console.log('Village: ' + village.getId() + ', Target: ' + villageId + ', Preset: ' + FakePresets[4])
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake5, interval * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake5() {
                if (!FakePresets[5]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[5].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[5] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                socketService.emit(routeProvider.SEND_PRESET, {
                                    start_village: village.getId(),
                                    target_village: villageId,
                                    army_preset_id: FakePresets[5],
                                    type: 'attack'
                                })
                                console.log('Village: ' + village.getId() + ', Target: ' + villageId + ', Preset: ' + FakePresets[5])
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake6, interval * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake6() {
                if (!FakePresets[6]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[6].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[6] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                socketService.emit(routeProvider.SEND_PRESET, {
                                    start_village: village.getId(),
                                    target_village: villageId,
                                    army_preset_id: FakePresets[6],
                                    type: 'attack'
                                })
                                console.log('Village: ' + village.getId() + ', Target: ' + villageId + ', Preset: ' + FakePresets[6])
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake7, interval * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake7() {
                if (!FakePresets[7]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[7].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[7] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                socketService.emit(routeProvider.SEND_PRESET, {
                                    start_village: village.getId(),
                                    target_village: villageId,
                                    army_preset_id: FakePresets[7],
                                    type: 'attack'
                                })
                                console.log('Village: ' + village.getId() + ', Target: ' + villageId + ', Preset: ' + FakePresets[7])
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake8, interval * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake8() {
                if (!FakePresets[8]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[8].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[8] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                socketService.emit(routeProvider.SEND_PRESET, {
                                    start_village: village.getId(),
                                    target_village: villageId,
                                    army_preset_id: FakePresets[8],
                                    type: 'attack'
                                })
                                console.log('Village: ' + village.getId() + ', Target: ' + villageId + ', Preset: ' + FakePresets[8])
                            }, index * interval * Math.random())
                        }
                    })
                    setTimeout(Fake9, interval * Math.floor((Math.random() * 5) + 1))
                }
            }

            function Fake9() {
                if (!FakePresets[9]) {
                    return console.log('Brak szablonów fejkowych!')
                } else {
                    villages.forEach(function(village, index) {
                        var VID = village.getId()
                        if (!presetsAssigned[9].includes(VID)) {
                            return console.log('Szablon: ' + FakePresets[9] + ' nie przypisany do tej wioski: ' + VID)
                        } else {
                            setTimeout(function() {
                                socketService.emit(routeProvider.SEND_PRESET, {
                                    start_village: village.getId(),
                                    target_village: villageId,
                                    army_preset_id: FakePresets[9],
                                    type: 'attack'
                                })
                                console.log('Village: ' + village.getId() + ', Target: ' + villageId + ', Preset: ' + FakePresets[9])
                            }, index * interval * Math.random())
                        }
                    })
                }
            }
            Presets()
        })
        $rename.on('click', function() {
            var modelDataService = injector.get('modelDataService')
            var eventTypeProvider = injector.get('eventTypeProvider')
            var player = modelDataService.getSelectedCharacter()
            var villages = player.getVillageList()
            var newName = document.getElementById('rename').value
            var interval = 3000

            function renameVillages() {
                villages.forEach(function(village, index) {
                    setTimeout(function() {
                        socketService.emit(routeProvider.VILLAGE_CHANGE_NAME, {
                            village_id: village.getId(),
                            name: newName
                        })
                    }, index * interval)
                })
                utils.emitNotif('success', Locale('faker', 'rename-done'))
            }
            renameVillages()
        })
        $renameCount.on('click', function() {
            var modelDataService = injector.get('modelDataService')
            var eventTypeProvider = injector.get('eventTypeProvider')
            var player = modelDataService.getSelectedCharacter()
            var villages = player.getVillageList()
            var newName = document.getElementById('rename1').value
            var interval = 3000

            function renameVillages() {
                villages.forEach(function(village, index) {
                    var countNew = newName + ' ' + index  
                    setTimeout(function() {
                        socketService.emit(routeProvider.VILLAGE_CHANGE_NAME, {
                            village_id: village.getId(),
                            name: countNew
                        })
                    }, index * interval)
                })
                utils.emitNotif('success', Locale('faker', 'rename-done'))
            }
            renameVillages()
        })
    }
    var FakerInterface = function() {
        ui = new Interface('FakeSender', {
            activeTab: 'faker',
            template: '__faker_html_window',
            css: '__faker_css_style',
            replaces: {
                locale: Locale,
                version: FakeSender.version,
                types: ['village', 'character']
            }
        })
        opener = new FrontButton(Locale('faker', 'title'), {
            classHover: false,
            classBlur: false,
            onClick: function() {
                ui.openWindow()
            }
        })
        $window = $(ui.$window)
        $fourOfAKind = $window.find('.item-send span')
        $fakePlayer = $window.find('.item-send1 span')
        $fakeVillage = $window.find('.item-send2 span')
        $rename = $window.find('.item-rename span')
        $renameCount = $window.find('.item-rename1 span')
        bindEvents()
        FakeSender.interfaceInitialized = true
        return ui
    }
    FakeSender.interface = function() {
        FakeSender.interface = FakerInterface()
    }
})