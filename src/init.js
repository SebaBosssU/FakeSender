require([
    'two/ready',
    'two/faker',
    'two/faker/ui'
], function (
    ready,
    FakeSender
) {
    if (FakeSender.initialized) {
        return false
    }

    ready(function () {
        FakeSender.init()
        FakeSender.interface()
        FakeSender.run()
    })
})
