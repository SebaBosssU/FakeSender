define('two/faker', [
    'two/locale',
    'two/ready'
], function (
    Locale,
    ready
) {
    var FakeSender = {}
    FakeSender.version = '__faker_version'
    FakeSender.init = function () {
        Locale.create('faker', __faker_locale, 'pl')

        FakeSender.initialized = true
    }
    FakeSender.run = function () {
        if (!FakeSender.interfaceInitialized) {
            throw new Error('FakeSender interface not initialized')
        }
        ready(function () {
            $player = modelDataService.getSelectedCharacter()
        }, ['initial_village'])
    }
    return FakeSender
})